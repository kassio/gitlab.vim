local utils = {}
local fn = vim.fn
local loop = vim.loop

function utils.user_data_path()
  return fn.stdpath('data')
end

function utils.formatted_line_for_print(m)
  if m == '' then
    return ''
  end

  return string.format('GCS: %s', m)
end

function utils.current_os()
  local res = fn.system({ 'uname', '-s' })
  res = string.gsub(res, '%s+', '')

  return string.lower(res)
end

function utils.current_arch()
  local res = fn.system({ 'uname', '-m' })
  res = string.gsub(res, '%s+', '')

  if res == 'arm64' then
    res = 'amd64'
  end

  return string.lower(res)
end

function utils.path_exists(path)
  return loop.fs_stat(path)
end

function utils.exec_cmd(cmd, cb)
  local stdout = ''
  local stderr = ''

  return fn.jobstart(cmd, {
    on_stdout = function(_job_id, data, _event)
      stdout = stdout .. fn.join(data)
    end,

    on_stderr = function(_job_id, data, _event)
      stderr = stderr .. fn.join(data)
    end,

    on_exit = function(_job_id, exit_code, _event)
      local result = { exit_code = exit_code, stdout = stdout, stderr = stderr, msg = '' }

      if exit_code ~= 0 then
        result.msg = string.format(
          'Error detected, stdout=[%s], stderr=[%s], code=[%s]',
          stdout,
          stderr,
          exit_code
        )
      end

      cb(result)
    end,
  })
end

return utils
