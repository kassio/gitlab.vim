local logging = {
  options = {
    debug = false,
    enabled = true,
    version = 'unknown',
  },
  path = vim.fn.stdpath('log') .. 'gitlab.vim.log',
}

local function format_line(msg, level, t)
  local timestamp = t or '!%Y-%m-%d %H:%M:%S'
  local line =
    string.format('%s: %s (%s): %s', os.date(timestamp), level, logging.options.version, msg)

  return line
end

local function log(msg, level)
  if logging.options.enabled ~= true then
    return
  end

  local log_file = assert(io.open(logging.path, 'a'))
  local fh = io.output(log_file)
  local formatted = format_line(msg, level) .. '\n'

  fh:write(formatted)
  fh:close()
end

function logging.info(msg)
  log(msg, 'INFO')
end

function logging.warn(msg)
  log(msg, 'WARN')
end

function logging.error(msg)
  log(msg, 'ERROR')
end

function logging.debug(msg, f)
  local force = f or false

  if force or logging.options.debug then
    log(msg, 'DEBUG')
  end
end

function logging.setup(options)
  logging.options = vim.tbl_deep_extend('force', logging.options, options)
end

return logging
