local version = '0.1.3'

local gitlab = {
  initialized = false,
  globals = require('gitlab.globals'),
  defaults = {
    logging = {
      version = version,
      debug = os.getenv('GITLAB_VIM_DEBUG') == '1',
      enabled = os.getenv('GITLAB_VIM_LOGGING') ~= '0',
    },
    statusline = {},
    authentication = {},
    code_suggestions = {
      enabled = true,
      personal_access_token = nil,
    },
  },
}

function gitlab.init(options)
  if not gitlab.initialized then
    gitlab.options = vim.tbl_deep_extend('force', gitlab.defaults, options)
    gitlab.options.code_suggestions = vim.tbl_deep_extend(
      'force',
      gitlab.defaults.code_suggestions,
      gitlab.options.code_suggestions
    )
  end
  gitlab.initialized = true

  if not gitlab.logging then
    gitlab.logging = require('gitlab.logging')
  end
  if not gitlab.statusline then
    gitlab.statusline = require('gitlab.statusline')
  end
  if not gitlab.authentication then
    gitlab.authentication = require('gitlab.authentication')
  end
  if not gitlab.code_suggestions then
    gitlab.code_suggestions = require('gitlab.code_suggestions')
  end

  return gitlab
end

function gitlab.setup(options)
  gitlab.init(options)

  gitlab.logging.setup(vim.tbl_deep_extend('force', gitlab.options.logging, { version = version }))
  gitlab.logging.info('Starting up..')

  if gitlab.options.code_suggestions.enabled then
    gitlab.statusline.setup(gitlab.globals.GCS_CHECKING)

    gitlab.code_suggestions.setup(gitlab.options.code_suggestions)

    if gitlab.authentication then
      gitlab.authentication.register(gitlab.statusline)
    end
  else
    gitlab.statusline.setup(gitlab.globals.GCS_UNAVAILABLE)
  end
end

return gitlab
